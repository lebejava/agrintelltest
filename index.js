//Si lees esto, significa que se actualizó correctamente
const moment = require('moment')
const db = require('mongoose')
const request = require('request')
const readline = require('readline')
const exec = require('child_process').exec
var CronJob = require('cron').CronJob
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

db.Promise = global.Promise
db.connect('mongodb://127.0.0.1:27017/database', {useNewUrlParser: true}, function(error) {
  if(!error) {
    interval = setInterval(function(){
      MongoDB.collection('registro').count({}, function(error, count) {
        if(count > 0) {
          request(servidor, function(error, response, body) {
            if(!error) {
              sincronizar()
              clearInterval(interval)
            }
          })
        }else{
          clearInterval(interval)
        }
      })
    }, 5000)
  }
});
var MongoDB = db.connection
var ObjectId = require('mongodb').ObjectID

const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const serialPort = new SerialPort('/dev/ttyS0', { baudRate: 115200 })
const parser = serialPort.pipe(new Readline({ delimiter: '\n' }))

var servidor = 'http://riegoiot.com'
//servidor = 'http://192.168.1.15:3000'
var interval

var secuencia = 0
var config = ['at+mode=1', 'at+rf_config=917000000,10,0,1,8,20', 'at+rxc=1']

var json = {
  coordinate: {
    coordinateid: 'gat1002',
    code: 3000,
    rssitx: parseInt(Math.random() * (50 - 0) + 0),
    rssirx: parseInt(Math.random() * (50 - 0) + 0),
    battery: parseInt(Math.random() * (80 - 0) + 0),
    latitude: 12000,
    longitude: 12000,
    state: 1,
    date: moment().format("YYYY-MM-DD HH:mm:ss.SSS") + "z"
  },
  device: {}
}

//DEBUG: setTimeout(function(){ procesarDatos("1000/26877463/20/20/100/-16,0/-71,0.100/200/300/400.1/2/3.") }, 3000)

//DATA: "rxdata" + code/id/rssirx/rssitx/battery/lat/lng.t1/t2/t3/t4.conductividad/temperatura/volume_content.temperatura_station/humidity/rain/speed_wind/light/presure/bat2")
var linea = ''
function conectarSerial() {
  serialPort.on('open', function () {
    serialPort.on('data', function (data) {
      linea += data
      if(linea[linea.length-1] == '\n') {
        console.log(linea)
        if(linea.includes('rxdata')) {
          var payload = linea.substring(linea.indexOf('rxdata')+6, linea.length-2)
          procesarDatos(payload)
        }
        linea = ''
      }
    })
  })

  serialPort.on('close', function() {
    console.log('Serial: Puerto cerrado')
    setTimeout(function() {
      conectarSerial()
    }, 2000)
  })

  serialPort.on('error', function (err) {
    console.error('Serial:', err)
    setTimeout(function() {
      conectarSerial()
    }, 2000)
  })
}
conectarSerial()

var lastUpdate = moment().unix()

function procesarDatos(payload) {
  lastUpdate = moment().unix()
  console.log("Payload:", payload)
  payload = payload.split('.')
  //console.log('Data 0', payload[0]);
  //console.log('Data 1', payload[1]);
  //console.log('Data 2', payload[2]);
  //console.log('Data 3', payload[3]);
  switch (payload[0].split('/')[0]) {
    case '1000':
      if(payload.length == 4) {
        json.device.code = payload[0].split('/')[0] ? payload[0].split('/')[0] : ''
        json.device.devid = payload[0].split('/')[1] ? payload[0].split('/')[1] : ''
        json.device.rssirx = payload[0].split('/')[2] ? payload[0].split('/')[2] : ''
        json.device.rssitx = payload[0].split('/')[3] ? payload[0].split('/')[3] : ''
        json.device.battery = payload[0].split('/')[4] ? payload[0].split('/')[4] : ''
        json.device.latitude = payload[0].split('/')[5] ? payload[0].split('/')[5] : ''
        json.device.longitude = payload[0].split('/')[6] ? payload[0].split('/')[6] : ''
        json.device.state = 1

        json.device.tensiometer1 = payload[1].split('/')[0] ? payload[1].split('/')[0] : 0
        json.device.tensiometer2 = payload[1].split('/')[1] ? payload[1].split('/')[1] : 0
        json.device.tensiometer3 = payload[1].split('/')[2] ? payload[1].split('/')[2] : 0
        json.device.tensiometer4 = payload[1].split('/')[3] ? payload[1].split('/')[3] : 0

        json.device.conductivity = payload[2].split('/')[0] ? payload[2].split('/')[0] : 0
        json.device.temperature = payload[2].split('/')[1] ? payload[2].split('/')[1] : 0
        json.device.volume_content = payload[2].split('/')[2] ? payload[2].split('/')[2] : 0

        json.device.temperature_station = payload[3].split('/')[0] ? payload[3].split('/')[0] : 0
        json.device.humidity = payload[3].split('/')[1] ? payload[3].split('/')[1] : 0
        json.device.rain = payload[3].split('/')[2] ? payload[3].split('/')[2] : 0
        json.device.speed_wind = payload[3].split('/')[3] ? payload[3].split('/')[3] : 0
        json.device.light = payload[3].split('/')[4] ? payload[3].split('/')[4] : 0
        json.device.pressure_air = payload[3].split('/')[5] ? payload[3].split('/')[5] : 0
        json.device.battery2 = payload[3].split('/')[6] ? payload[3].split('/')[6] : 0
        delete json._id
        json.coordinate.date = moment().format("YYYY-MM-DD HH:mm:ss.SSS") + "z"
        enviarDatos(json, 0)
      }
      break;
      case '2000':
      //rxdata2000/id/rssirx/rssitx/battery/lat/lng/1-0/state
      //txdata3000/2000/id/state/mask
      /*json.device.code = payload[0].split('/')[0] ? payload[0].split('/')[0] : ''
      json.device.devid = payload[0].split('/')[1] ? payload[0].split('/')[1] : ''
      json.device.rssirx = payload[0].split('/')[2] ? payload[0].split('/')[2] : ''
      json.device.rssitx = payload[0].split('/')[3] ? payload[0].split('/')[3] : ''
      json.device.battery = payload[0].split('/')[4] ? payload[0].split('/')[4] : ''
      json.device.latitude = payload[0].split('/')[5] ? payload[0].split('/')[5] : ''
      json.device.longitude = payload[0].split('/')[6] ? payload[0].split('/')[6] : ''
      json.device.state = payload[0].split('/')[8] ? payload[0].split('/')[8] : ''
      if(payload[0].split('/')[7] == '1') {
        serialPort.write('txdata3000/2000/' + payload[0].split('/')[1] + '/170/170\r\n', (err) => {
          if (err) {
            return console.log('Error escribiendo: ', err.message)
          }
          console.log('mensaje enviado')
        })
      }else{
        //confirmación
      }
      delete json._id
      json.coordinate.date = moment().format("YYYY-MM-DD HH:mm:ss.SSS") + "z"
      enviarDatos(json, 0)*/
      break
  }
}

function enviarDatos(data, state) {
  console.log('ENVIANDO...:', data)
  var options = {
    url: servidor + '/api/v1/device/devices-add-local',
    method: 'POST',
    timeout: 4000
  }

  options.json = data

  request(options, function(error, response, body) {
    if(error || body.status == 'error') {
      console.log('Error Request:', (error ? error : body))
      if(state == 0) {
        MongoDB.collection('registro').insert(data, function(error, ok) {
          if(error) {
            console.log('Error MongoDB:', error)
          }else{
            console.log('GUARDADO OK')
          }
        })
      }
      clearInterval(interval)
      interval = setInterval(function(){
        MongoDB.collection('registro').count({}, function(error, count) {
          if(count > 0) {
            request(servidor, function(error, response, body) {
              if(!error) {
                sincronizar()
                clearInterval(interval)
              }
            })
          }else{
            clearInterval(interval)
          }
        })
      }, 5000)
    }else{
      console.log('ENVIADO OK:', body)
      if(state != 0) {
        MongoDB.collection('registro').remove({ '_id' : db.Types.ObjectId(state)}, function(error, removed) {
          if(error) {
            console.log('Error MongoDB:', error)
          }else{
            console.log('BORRADO:', state)
          }
        })
      }
    }
  })
}

function sincronizar() {
  console.log('SINCRONIZANDO')
  MongoDB.collection('registro').find().toArray(function(error, find) {
    if(find.length > 0) {
      for(var i = 0; i < find.length; i++) {
        enviarDatos(find[i], find[i]._id)
      }
    }
  })
}

function restart(callback) {
  exec('pm2 restart all --watch', function(error, stdout, stderr){ callback(stdout) })
}

function update(callback) {
  exec('git pull origin master', function(error, stdout, stderr){ callback(stdout) })
}

function shutdown(callback) {
  exec('sudo reboot', function(error, stdout, stderr){ callback(stdout) })
}
//Tareas

new CronJob('00 00 * * * *', function() {
  var diffTime = moment().diff(lastUpdate);
  var duration = moment.duration(diffTime);
  if(duration.minutes() >= 10) {
    restart(function(output) {
      console.log(output)
    })
  }
}, null, true, 'America/Lima')

new CronJob('00 15 * * * *', function() {
  var diffTime = moment().diff(lastUpdate);
  var duration = moment.duration(diffTime);
  if(duration.minutes() >= 10) {
    restart(function(output) {
      console.log(output)
    })
  }
}, null, true, 'America/Lima')

new CronJob('00 30 * * * *', function() {
  var diffTime = moment().diff(lastUpdate);
  var duration = moment.duration(diffTime);
  if(duration.minutes() >= 10) {
    restart(function(output) {
      console.log(output)
    })
  }
}, null, true, 'America/Lima')

new CronJob('00 45 * * * *', function() {
  var diffTime = moment().diff(lastUpdate);
  var duration = moment.duration(diffTime);
  if(duration.minutes() >= 10) {
    restart(function(output) {
      console.log(output)
    })
  }
}, null, true, 'America/Lima')

new CronJob('00 00 00 * * *', function() {
  MongoDB.collection('registro').remove({orderExpDate: {"$lt": new Date(Date.now() - 7*24*60*60 * 1000)}}, function(error, removed) {
    shutdown(function(output) {
      console.log(output)
    })
  })
}, null, true, 'America/Lima')
